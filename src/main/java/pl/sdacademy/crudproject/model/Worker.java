package pl.sdacademy.crudproject.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity (name = "worker")

@Data
@NoArgsConstructor

public class Worker {
    @Id
    @Column (name = "worker_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int workerId;

    @Column (name = "first_name")
    private String firstName;

    @Column (name = "last_name")
    private String lastName;

    @Column (name = "age")
    private int age;

    @Column (name = "hire_date")
    private LocalDate hireDate;

    @ManyToOne
    @JoinColumn(name = "department_id")
    private Department department;

    public Worker(String firstName, String lastName, int age, String hireDate, Department department) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.hireDate = LocalDate.parse(hireDate);
        this.department = department;
    }

    public Worker(int workerId, String firstName, String lastName, int age, LocalDate hireDate, Department department) {
        this.workerId = workerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.hireDate = hireDate;
        this.department = department;
    }

    @Override
    public String toString() {
        return "Worker: " +
                "workerId [" + workerId +
                "], firstName=['" + firstName + '\'' +
                "], lastName=['" + lastName + '\'' +
                "], age=[" + age +
                "], hireDate=[" + hireDate +
                "], department=[" + department.getDepartmentId() +
                ']';
    }
}
