package pl.sdacademy.crudproject.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity (name = "department")

@Data
@NoArgsConstructor
@AllArgsConstructor


public class Department {
    @Id
    @Column (name = "department_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int departmentId;

    @Column (name = "name")
    private String name;

    public Department(String name) {
        this.name = name;
    }

    public Department(int departmentId) {
        this.departmentId = departmentId;
    }
    /* @OneToMany
    //@JoinColumn(name = "department_id")
    private List<Worker> workers;*/

    @Override
    public String toString() {
        return "Department: " +
                "departmentId=[" + departmentId +
                "], name=['" + name + '\'' +
                ']';
    }
}
