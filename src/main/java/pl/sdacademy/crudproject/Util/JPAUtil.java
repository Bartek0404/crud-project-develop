package pl.sdacademy.crudproject.Util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.function.Consumer;

public class JPAUtil {
    
    private static  EntityManager entityManager = null;
    private static  EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("CRUD_JPA");
    
    public static EntityManager getEntityManager() {
        if(entityManager == null) {
            entityManager = entityManagerFactory.createEntityManager();
        }
        return entityManager;
    }
    public static void doInTransaction(Consumer<EntityManager> action) {
        entityManager = getEntityManager();
        EntityTransaction entityTransaction = null;
        try {
            entityTransaction = entityManager.getTransaction();
            entityTransaction.begin();
            action.accept(entityManager);
            entityTransaction.commit();
        } catch (RuntimeException exception) {
            entityTransaction.rollback();
        }
    }

}
