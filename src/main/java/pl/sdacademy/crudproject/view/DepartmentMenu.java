package pl.sdacademy.crudproject.view;

import pl.sdacademy.crudproject.view.pages.*;

import java.util.Arrays;
import java.util.List;

public class DepartmentMenu extends AbstractMenu {
    @Override
    public List<MenuItem> getMenuItems() {
        return Arrays.asList(
                new MenuItem("Dodaj departament", 1, new DepartmentMenu(), new CreateDepartmentPage()),
                new MenuItem("Wypisz dane departamentu", 2, new DepartmentMenu(), new ListDepartmentByIdPage()),
                new MenuItem("Aktualizuj dane departamentu", 3, new DepartmentMenu(), new UpdateDepartmentPage()),
                new MenuItem("Wypisz departamenty", 4, new DepartmentMenu(), new ListAllDepartmentsPage()),
                new MenuItem("Usuń departament", 5, new DepartmentMenu(), new DeleteDepartmentPage()),
                new MenuItem("Powrót", 0, new Menu(), null)
        );
    }
}
