package pl.sdacademy.crudproject.view.pages;

import pl.sdacademy.crudproject.dao.DaoFactory;
import pl.sdacademy.crudproject.dao.DepartmentDao;
import pl.sdacademy.crudproject.dao.WorkerDao;
import pl.sdacademy.crudproject.model.Department;
import pl.sdacademy.crudproject.model.Worker;
import pl.sdacademy.crudproject.view.ViewUtil;

import java.util.Scanner;

public class CreateWorkerPage implements Page {
    @Override
    public void show() {
        System.out.println("Dodawanie nowego pracownika:");
        Scanner scanner = ViewUtil.getScanner();
        System.out.println("Podaj imię nowego pracownika: ");
        String firstName = scanner.nextLine();
        System.out.println("Podaj nazwisko nowego pracownika:");
        String lastName = scanner.nextLine();
        System.out.println("Podaj wiek nowego Pracownika:");
        int age = scanner.nextInt();
        scanner.nextLine();
        System.out.println("Podaj datę zatrudnienia nowego Pracownika w formacie YYYY-MM-DD:");
        String hireDate = scanner.nextLine();

        DepartmentDao dDao = (DepartmentDao) DaoFactory.get(DepartmentDao.class);
        System.out.println("Podaj ID Deprtamentu dla nowego Pracownika:");
        int departmentId;
        boolean isDepartmentValid = false;
        do {
            departmentId = scanner.nextInt();
            scanner.nextLine();
            if (dDao.get(departmentId).isEmpty()) {
                System.out.println("Brak departamentu o podanym ID");
                System.out.println("Wprowadź ID z listy poniżej: ");
                System.out.println(dDao.getAll());
            }
            else {
                isDepartmentValid = true;
            }
        } while (!isDepartmentValid);

        WorkerDao wDao = (WorkerDao) DaoFactory.get(WorkerDao.class);
        Worker newWorker = new Worker(firstName, lastName, age, hireDate, new Department(departmentId));
        System.out.println("Utworzono pracownika: " + wDao.create(newWorker));

        /*//Stare bez Optionala
        Department department = dDao.get(departmentID);
        Worker newWorker = new Worker(firstName, lastName, age, hireDate, department);
        Worker createdWorker = wDao.create(newWorker);
        System.out.println("Utworzono: " + createdWorker);*/
    }
}
