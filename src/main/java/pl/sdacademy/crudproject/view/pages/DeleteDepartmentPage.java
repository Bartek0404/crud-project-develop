package pl.sdacademy.crudproject.view.pages;

import pl.sdacademy.crudproject.dao.DaoFactory;
import pl.sdacademy.crudproject.dao.DepartmentDao;
import pl.sdacademy.crudproject.view.ViewUtil;
import java.util.Scanner;

public class DeleteDepartmentPage implements Page {
    @Override
    public void show() {
        System.out.println("Kasowanie departamentu po ID");
        Scanner scanner = ViewUtil.getScanner();
        System.out.println("Podaj ID departamentu:");
        int id = scanner.nextInt();
        scanner.nextLine();
        DepartmentDao dao = (DepartmentDao) DaoFactory.get(DepartmentDao.class);
        dao.delete(id);
    }
}
