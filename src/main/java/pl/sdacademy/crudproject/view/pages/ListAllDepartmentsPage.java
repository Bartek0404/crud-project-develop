package pl.sdacademy.crudproject.view.pages;

import pl.sdacademy.crudproject.dao.DaoFactory;
import pl.sdacademy.crudproject.dao.DepartmentDao;
import pl.sdacademy.crudproject.model.Department;
import pl.sdacademy.crudproject.view.ViewUtil;

import java.util.List;
import java.util.Scanner;

public class ListAllDepartmentsPage implements Page {
    @Override
    public void show() {
        System.out.println("Dane wszystkich departamentów:");
        DepartmentDao dao = (DepartmentDao) DaoFactory.get(DepartmentDao.class);
        List<Department> departmentList = dao.getAll();
        for (Department department : departmentList) {
            System.out.println(department);
        }
    }
}
