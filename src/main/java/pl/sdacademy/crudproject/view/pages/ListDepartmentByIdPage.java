package pl.sdacademy.crudproject.view.pages;

import pl.sdacademy.crudproject.dao.DaoFactory;
import pl.sdacademy.crudproject.dao.DepartmentDao;
import pl.sdacademy.crudproject.view.ViewUtil;

import java.util.Scanner;

public class ListDepartmentByIdPage implements Page {
    @Override
    public void show() {
        System.out.println("Wypisz departament po ID");
        Scanner scanner = ViewUtil.getScanner();
        System.out.println("Podaj ID departamentu:");
        int id = scanner.nextInt();
        scanner.nextLine();
        DepartmentDao dao = (DepartmentDao) DaoFactory.get(DepartmentDao.class);
        if (dao.get(id).isPresent()){
            System.out.println("Dane departamentu: " + dao.get(id).get());
        }
        else {
            System.out.println("Brak departamentu o podanym ID: " + id);
        }

        /*//Stare bez Optionala
        System.out.println("Dane departamentu: " + dao.get(id));*/
    }
}
