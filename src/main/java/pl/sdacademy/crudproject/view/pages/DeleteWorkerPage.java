package pl.sdacademy.crudproject.view.pages;

import pl.sdacademy.crudproject.dao.DaoFactory;
import pl.sdacademy.crudproject.dao.WorkerDao;
import pl.sdacademy.crudproject.view.ViewUtil;

import java.util.Scanner;

public class DeleteWorkerPage implements Page {
    @Override
    public void show() {
        System.out.println("Kasowanie pracownika po ID");
        Scanner scanner = ViewUtil.getScanner();
        System.out.println("Podaj ID pracownika:");
        int id = scanner.nextInt();
        scanner.nextLine();
        WorkerDao dao = (WorkerDao) DaoFactory.get(WorkerDao.class);
        dao.delete(id);
    }
}
