package pl.sdacademy.crudproject.view.pages;

import pl.sdacademy.crudproject.dao.DaoFactory;
import pl.sdacademy.crudproject.dao.WorkerDao;
import pl.sdacademy.crudproject.model.Worker;

import java.util.List;

public class ListAllWorkersPage implements Page {
    @Override
    public void show() {
        System.out.println("Dane wszystkich pracowników:");
        WorkerDao dao = (WorkerDao) DaoFactory.get(WorkerDao.class);
        List<Worker> workerList = dao.getAll();
        for (Worker worker : workerList) {
            System.out.println(worker);
        }
    }
}
