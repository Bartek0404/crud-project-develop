package pl.sdacademy.crudproject.view.pages;

import pl.sdacademy.crudproject.dao.DaoFactory;
import pl.sdacademy.crudproject.dao.DepartmentDao;
import pl.sdacademy.crudproject.model.Department;
import pl.sdacademy.crudproject.view.ViewUtil;

import java.util.Optional;
import java.util.Scanner;

public class UpdateDepartmentPage implements Page {
    @Override
    public void show() {
        System.out.println("Zmień dane Departamentu");
        Scanner scanner = ViewUtil.getScanner();
        System.out.println("Podaj ID Departamentu:");
        int id = scanner.nextInt();
        scanner.nextLine();
        DepartmentDao dao = (DepartmentDao) DaoFactory.get(DepartmentDao.class);
        Optional<Department> optionalDepartment = dao.get(id);
        if (optionalDepartment.isPresent()){
            Department updatedDepartment = optionalDepartment.get();
            System.out.println("Dane departamentu: " + updatedDepartment);
            System.out.println("Podaj nową nazwę Departamentu lub naciśnij ENTER aby zachować obecną: " + updatedDepartment.getName());
            String name = scanner.nextLine();
            if (!name.equals("")) {
                updatedDepartment.setName(name);
            }
            dao.update(updatedDepartment);
        }
        else {
            System.out.println("Brak departamentu o podanym ID: " + id);
        }

        /*//Stare bez Optionala
        Department updatedDepartment = dao.get(id);
        System.out.println("Zmieniasz dane Departamentu: " + updatedDepartment);
        System.out.println("Podaj nową nazwę Departamentu lub naciśnij ENTER aby zachować obecną: " + updatedDepartment.getName());
        String name = scanner.nextLine();
        if (!name.equals("")) {
            updatedDepartment.setName(name);
        }
        dao.update(updatedDepartment);*/
    }
}
