package pl.sdacademy.crudproject.view.pages;

import pl.sdacademy.crudproject.dao.DaoFactory;
import pl.sdacademy.crudproject.dao.WorkerDao;
import pl.sdacademy.crudproject.view.ViewUtil;

import java.util.Scanner;

public class ListWorkerByIdPage implements Page {
    @Override
    public void show() {
        System.out.println("Wypisz dane pracownika po ID");
        Scanner scanner = ViewUtil.getScanner();
        System.out.println("Podaj ID pracownika:");
        int id = scanner.nextInt();
        scanner.nextLine();
        WorkerDao dao = (WorkerDao) DaoFactory.get(WorkerDao.class);
        if (dao.get(id).isPresent()){
            System.out.println("Dane pracownika: " + dao.get(id).get());
        }
        else {
            System.out.println("Brak pracownika o podanym ID: " + id);
        }

        /*//Stare bez Optionala
        System.out.println("Dane pracownika: " + dao.get(id));*/
    }
}
