package pl.sdacademy.crudproject.view.pages;

import pl.sdacademy.crudproject.dao.DaoFactory;
import pl.sdacademy.crudproject.dao.DepartmentDao;
import pl.sdacademy.crudproject.dao.WorkerDao;
import pl.sdacademy.crudproject.model.Department;
import pl.sdacademy.crudproject.model.Worker;
import pl.sdacademy.crudproject.view.ViewUtil;

import java.time.LocalDate;
import java.util.Optional;
import java.util.Scanner;

public class UpdateWorkerPage implements Page {
    @Override
    public void show() {
        System.out.println("Zmień dane pracownika");
        Scanner scanner = ViewUtil.getScanner();
        System.out.println("Podaj ID pracownika:");
        int id = scanner.nextInt();
        scanner.nextLine();
        WorkerDao wDao = (WorkerDao) DaoFactory.get(WorkerDao.class);
        Optional<Worker> optionalWorker = wDao.get(id);
        if (optionalWorker.isPresent()) {
            Worker updatedWorker = optionalWorker.get();
            System.out.println("Zmieniasz dane pracownika: " + updatedWorker);
            System.out.println("Podaj nowe imię pracownika lub naciśnij ENTER aby zachować obecne: " + updatedWorker.getFirstName());
            String firstName = scanner.nextLine();
            if (!firstName.equals("")) {
                updatedWorker.setFirstName(firstName);
            }
            System.out.println("Podaj nowe nazwisko pracownika lub naciśnij ENTER aby zachować obecne: " + updatedWorker.getLastName());
            String lastName = scanner.nextLine();
            if (!lastName.equals("")) {
                updatedWorker.setLastName(lastName);
            }
            System.out.println("Podaj nowy wiek pracownika lub naciśnij ENTER aby zachować obecny: " + updatedWorker.getAge());
            String age = scanner.nextLine();
            if (!age.equals("")) {
                updatedWorker.setAge(Integer.parseInt(age));
            }
            System.out.println("Podaj nową datę zatrudnienia pracownika w formacie YYYY-MM-DD lub naciśnij ENTER aby zachować obecną: " + updatedWorker.getHireDate());
            String hireDate = scanner.nextLine();
            if (!hireDate.equals("")) {
                updatedWorker.setHireDate(LocalDate.parse(hireDate));
            }

            DepartmentDao dDao = (DepartmentDao) DaoFactory.get(DepartmentDao.class);
            System.out.println("Podaj nowy ID Deprtamentu dla pracownika lub naciśnij ENTER aby zachować obecny: " + updatedWorker.getDepartment().getDepartmentId());
            boolean isDepartmentValid = false;
            do {
                String departmentId = scanner.nextLine();
                if (!departmentId.equals("")) {
                    if (dDao.get(Integer.parseInt(departmentId)).isEmpty()) {
                        System.out.println("Brak departamentu o podanym ID");
                        System.out.println("Wprowadź ID z listy poniżej lub wciśnij ENTER aby zachować obecny: " + updatedWorker.getDepartment().getDepartmentId());
                        System.out.println(dDao.getAll());
                    }
                    else {
                        updatedWorker.setDepartment(new Department(Integer.parseInt(departmentId)));
                        isDepartmentValid = true;
                    }
                }
                else
                {
                    isDepartmentValid = true;
                }
            } while (!isDepartmentValid);

            wDao.update(updatedWorker);
        }



        /*//Stare bez Optionala
        Worker updatedWorker = wDao.get(id);
        System.out.println("Zmieniasz dane pracownika: " + updatedWorker);
        System.out.println("Podaj nowe imię pracownika lub naciśnij ENTER aby zachować obecne: " + updatedWorker.getFirstName());
        String firstName = scanner.nextLine();
        if (!firstName.equals("")) {
            updatedWorker.setFirstName(firstName);
        }
        System.out.println("Podaj nowe nazwisko pracownika lub naciśnij ENTER aby zachować obecne: " + updatedWorker.getLastName());
        String lastName = scanner.nextLine();
        if (!lastName.equals("")) {
            updatedWorker.setLastName(lastName);
        }
        System.out.println("Podaj nowy wiek pracownika lub naciśnij ENTER aby zachować obecny: " + updatedWorker.getAge());
        String age = scanner.nextLine();
        if (!age.equals("")) {
            updatedWorker.setAge(Integer.parseInt(age));
        }
        System.out.println("Podaj nową datę zatrudnienia pracownika w formacie YYYY-MM-DD lub naciśnij ENTER aby zachować obecną: " + updatedWorker.getHireDate());
        String hireDate = scanner.nextLine();
        if (!hireDate.equals("")) {
            updatedWorker.setHireDate(LocalDate.parse(hireDate));
        }
        System.out.println("Podaj nowy ID Deprtamentu dla pracownika lub naciśnij ENTER aby zachować obecny: " + updatedWorker.getDepartment().getDepartmentId());
        String departmentId = scanner.nextLine();
        if (!departmentId.equals("")) {
            updatedWorker.setDepartment(new Department(Integer.parseInt(departmentId)));
        }
        wDao.update(updatedWorker);*/
    }
}
