package pl.sdacademy.crudproject.view;

import pl.sdacademy.crudproject.view.pages.*;

import java.util.Arrays;
import java.util.List;

public class WorkerMenu extends AbstractMenu {
    @Override
    public List<MenuItem> getMenuItems() {
        return Arrays.asList(
                new MenuItem("Dodaj pracownika", 1, new WorkerMenu(), new CreateWorkerPage()),
                new MenuItem("Wypisz dane pracownika", 2, new WorkerMenu(), new ListWorkerByIdPage()),
                new MenuItem("Aktualizuj dane pracownika", 3, new WorkerMenu(), new UpdateWorkerPage()),
                new MenuItem("Wypisz pracowników", 4, new WorkerMenu(), new ListAllWorkersPage()),
                new MenuItem("Usuń pracownika", 5, new WorkerMenu(), new DeleteWorkerPage()),

                new MenuItem("Powrót", 0, new Menu(), null)
        );
    }
}
