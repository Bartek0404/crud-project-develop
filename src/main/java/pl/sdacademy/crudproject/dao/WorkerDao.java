package pl.sdacademy.crudproject.dao;

import pl.sdacademy.crudproject.Util.JPAUtil;
import pl.sdacademy.crudproject.model.Worker;
import java.util.List;
import java.util.Optional;

public class WorkerDao implements GenericDao<Worker> {

    @Override
    public Optional<Worker> get(int id) {
        Optional<Worker> optionalWorker = Optional.ofNullable(JPAUtil.getEntityManager().find(Worker.class, id));
        return optionalWorker;

        /*//Stara wersja bez Optionala
        Worker worker = JPAUtil.getEntityManager().find(Worker.class, id);
        if (worker == null) {
            throw new EntityNotFoundException("Can't find Worker for ID = " + id);
        }
        return worker;*/
    }

    @Override
    public List<Worker> getAll() {
        return JPAUtil.getEntityManager().createQuery("FROM worker ", Worker.class).getResultList();
    }

    @Override
    public void delete(int id) {
        Worker worker = JPAUtil.getEntityManager().find(Worker.class, id);
        if (worker!= null) {
            JPAUtil.doInTransaction(entityManager -> entityManager.remove(worker));
        }
        else {
            System.out.println("Brak pracownika o podanym ID: " + id);
            /*//Stara wersja z throwem
            throw new EntityNotFoundException("Can't find Worker for ID = " + id);*/
        }
    }

    @Override
    public Worker create(Worker entity) {
        JPAUtil.doInTransaction(entityManager -> entityManager.persist(entity));
        return entity;
    }

    @Override
    public Worker update(Worker entity) {
        JPAUtil.doInTransaction(entityManager -> entityManager.merge(entity));
        return entity;
    }
}
