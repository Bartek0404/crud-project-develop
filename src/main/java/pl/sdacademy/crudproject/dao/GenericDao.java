package pl.sdacademy.crudproject.dao;

import java.util.List;
import java.util.Optional;

public interface GenericDao<T> {
    Optional<T> get(int id);

    List<T> getAll();

    void delete(int id);

    T create(T entity);

    T update(T entity);

}
