package pl.sdacademy.crudproject.dao;

import pl.sdacademy.crudproject.Util.JPAUtil;
import pl.sdacademy.crudproject.model.Department;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

//@Slf4j
public class DepartmentDao implements GenericDao<Department> {

    @Override
    public Optional<Department> get(int id) {
        Optional<Department> optionalDepartment = Optional.ofNullable(JPAUtil.getEntityManager().find(Department.class, id));
        return optionalDepartment;
        //return optionalDepartment;

        /*//Stara wersja bez Optionala
        Department department = JPAUtil.getEntityManager().find(Department.class, id);
        if (department == null) {
            throw new EntityNotFoundException("Can't find Department for ID = " + id);
        }
        return department;*/
    }

    @Override
    public List<Department> getAll() {
        return JPAUtil.getEntityManager().createQuery("FROM department ", Department.class).getResultList();
    }

    @Override
    public void delete(int id) {

        Department department = JPAUtil.getEntityManager().find(Department.class, id);
        if (department!= null) {
            JPAUtil.doInTransaction(entityManager -> entityManager.remove(department));
        }
        else {
            System.out.println("Brak departamentu o podanym ID: " + id);
            //Stara wersja z throwem
            //throw new EntityNotFoundException("Can't find Department for ID = " + id);
        }
    }

    @Override
    public Department create(Department entity) {
        JPAUtil.doInTransaction(entityManager -> entityManager.persist(entity));
        return entity;
    }

    @Override
    public Department update(Department entity) {
        JPAUtil.doInTransaction(entityManager -> entityManager.merge(entity));
        return entity;
    }
}
